# Build your own!

You want to build your hardware by yourself, you are at the good place!

![finished](.docs/finished.png)

[TOC]


## Requirements (and shopping list)

1. Step one ![](./docs/1.png)
2. Step two ![](./docs/2.png)

## Part 2

1. Step one ![](./docs/3.png)
2. Step two ![](./docs/4.png)

## Part 3

1. Step one ![](./docs/5.png)
2. Step two ![](./docs/6.png)
