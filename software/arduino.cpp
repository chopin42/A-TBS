/*
 * This script is fully functional and can support 2 trains at the same time... Do not use with more because the system will be automaticaly stopped.
 *
 * On the hardware side there is a lot of wire connectors because I don't have enough "GND" pins on the arduino, so I decided to use wire conenectors to get more.
 * For other informations about hardware, please check the Fritzing file.
 *
 * To know how the led must be connected to the right pin on the Arduino, look at the variables and the legend bellow. Every "const char" variables are pins...
 *
 * How to read the variables:
 * F1A = FREINAGE, SECTION 1, PREMIER RELAIS
 * F1B = FREINAGE, SECTION 1, DEUXIEME RELAIS
 * S1 = SECURITÉ, SECTION 1
 *
 * sensor1A = Checkin / checkout sensor --> Block 1
 * sensor1B = Stopping if the next one is occuped --> Block 1
 *
 * L1G = Light 1 Green
 * L1O = Light 1 orange
 *
 * Remminder! With F?A, F?B and S? LOW is AC and HIGH is DC or nothing.
 */


// Getting the input from Serial Monitor
char rx_byte = 0;
String rx_str = "";

// Set the relays
// The octarelay > LOW = ON, HIGH = OFF
const char F1A = 13;
const char F1B = 12;
const char S1 = 11;

const char F2A = 10;
const char F2B = 9;
const char S2 = 8;

const char F3A = 7;
const char F3B = 6;
const char S3 = 5; // Is an alone relay so invert : LOW = OFF, HIGH = ON

const char F4A = 4;
const char F4B = 3;
const char S4 = 2;

//All the sensors
const char sensor1A = 22;
const char sensor1B = 24;

const char sensor2A = 26;
const char sensor2B = 28;

const char sensor3A = 30;
const char sensor3B = 32;

const char sensor4A = 34;
const char sensor4B = 36;

//All lights
const char L1R = 52;
const char L1O = 50;
const char L1G = 48;

const char L2R = 42;
const char L2O = 44;
const char L2G = 46;

const char L3R = 49;
const char L3O = 51;
const char L3G = 53;

const char L4R = 43;
const char L4O = 45;
const char L4G = 47;

// Set variables of every blocks
boolean stop1 = false;
boolean stop2 = false;
boolean stop3 = false;
boolean stop4 = false;

boolean block = true;
int detect = true;

void setup() {
  // Init the relays
  pinMode(F1A, OUTPUT);
  pinMode(F1B, OUTPUT);
  pinMode(S1, OUTPUT);

  pinMode(F2A, OUTPUT);
  pinMode(F2B, OUTPUT);
  pinMode(S2, OUTPUT);

  pinMode(F3A, OUTPUT);
  pinMode(F3B, OUTPUT);
  pinMode(S3, OUTPUT);

  pinMode(F4A, OUTPUT);
  pinMode(F4B, OUTPUT);
  pinMode(S4, OUTPUT);

  // Init the sensors
  pinMode(sensor1A,INPUT);
  pinMode(sensor1B,INPUT);
  pinMode(sensor2A,INPUT);
  pinMode(sensor2B,INPUT);
  pinMode(sensor3A,INPUT);
  pinMode(sensor3B,INPUT);
  pinMode(sensor4A,INPUT);
  pinMode(sensor4B,INPUT);

  //Init Lights
  pinMode(L1R, OUTPUT);
  pinMode(L1O, OUTPUT);
  pinMode(L1G, OUTPUT);

  pinMode(L2R, OUTPUT);
  pinMode(L2O, OUTPUT);
  pinMode(L2G, OUTPUT);

  pinMode(L3R, OUTPUT);
  pinMode(L3O, OUTPUT);
  pinMode(L3G, OUTPUT);

  pinMode(L4R, OUTPUT);
  pinMode(L4O, OUTPUT);
  pinMode(L4G, OUTPUT);

  //Reset lights

  digitalWrite(L1G, HIGH);
  digitalWrite(L2G, HIGH);
  digitalWrite(L3G, HIGH);
  digitalWrite(L4G, HIGH);

  //If not working set other LEDs to LOW


  // Init pins of Arduino
/*
  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
*/

  //Activating the circuit
  Serial.begin(9600);
  Serial.println("BLOCK SYSTEM = ON");
  seton();
  Serial.println("Seton...");

  //Activate block system
  block = true;
}

// -------------------------------------------------------------------------
// ---------------------------------------- LOOP ---------------------------
// -------------------------------------------------------------------------
void loop() {
  if (block == false) {
    orange();
  }

// ------------------------------------------------------- SERIAL INPUT BEGIN -------------------------------------------------------------------------
  if (Serial.available() > 0) {
    rx_byte = Serial.read();

    if (rx_byte != '\n') {
      rx_str += rx_byte;
    }
    else if(rx_str == "1") {
      Serial.println("BLOCK SYSTEM = OFF");
      seton();
      Serial.println("Seton...");

      //To power up the test mode set all lights to orange
      digitalWrite(L1O, HIGH);
      digitalWrite(L2O, HIGH);
      digitalWrite(L3O, HIGH);
      digitalWrite(L4O, HIGH);

      //And set the other ones to off
      digitalWrite(L1R, LOW);
      digitalWrite(L2R, LOW);
      digitalWrite(L3R, LOW);
      digitalWrite(L4R, LOW);

      digitalWrite(L1G, LOW);
      digitalWrite(L2G, LOW);
      digitalWrite(L3G, LOW);
      digitalWrite(L4G, LOW);

      // Set the track stop as false
      block = false;
      rx_str = "";
    }
    else if(rx_str == "0") {
      Serial.println("BLOCK SYSTEM = ON");
      seton();

      // Reset lights to green
      digitalWrite(L1G, HIGH);
      digitalWrite(L2G, HIGH);
      digitalWrite(L3G, HIGH);
      digitalWrite(L4G, HIGH);

      digitalWrite(L1O, LOW);
      digitalWrite(L2O, LOW);
      digitalWrite(L3O, LOW);
      digitalWrite(L4O, LOW);

      digitalWrite(L1R, LOW);
      digitalWrite(L2R, LOW);
      digitalWrite(L3R, LOW);
      digitalWrite(L4R, LOW);

      block = true;
      //Set the track stop as true
      //stop = true;
      rx_str = "";
    }
    else {
      Serial.println("Error 404 : code not found.");
      rx_str = "";
    }
  }
// ------------------------------------------------------- SERIAL INPUT ENDS -------------------------------------------------------------------------

if(block == true) {
  //Reads the following script:

// ------------------------------------------------------- BLOCK 1 BEGIN ----------------------------------------------------------------------------------
  //Set as occupied
  detect = digitalRead(sensor1A);
  if(detect == LOW) {
    stop4 = true;
    Serial.println("stop4 = true");

    //Set light to red
    digitalWrite(L4R, HIGH);
    digitalWrite(L4G, LOW);
    digitalWrite(L4O, LOW);

    //Set the 2 followings to the before-precedent block
    stop3 = false;
    digitalWrite(F3A, LOW);
    digitalWrite(F3B, LOW);
    digitalWrite(S3, LOW);

    //Set light to green
    digitalWrite(L3R, LOW);
    digitalWrite(L3G, HIGH);
    digitalWrite(L3O, LOW);
  }


  // Detecting the train to stop it
  int detect = digitalRead(sensor1B);

  //If the train is detected AND he need to stop THEN
  if(detect == LOW && stop1 == true) {
    //Serial.println("Stopping until restart");
    digitalWrite(F1A, HIGH);
    digitalWrite(F1B, HIGH);
    digitalWrite(S1, HIGH);
    stop1 = false;
  }
  //detect = 1;
// ------------------------------------------------------- BLOCK 1 ENDS ------------------------------------------------------------------------------------



// ------------------------------------------------------- BLOCK 2 BEGIN ----------------------------------------------------------------------------------
  //Set as occupied
  detect = digitalRead(sensor2A);
  if(detect == LOW) {
    //Set the 2 follwoings to the precedent block
    stop1 = true;
    Serial.println("stop1 = true");

    //Set light to red
    digitalWrite(L1R, HIGH);
    digitalWrite(L1G, LOW);
    digitalWrite(L1O, LOW);

    //Set the 2 followings to the before-precedent block
    stop4 = false;
    digitalWrite(F4A, LOW);
    digitalWrite(F4B, LOW);
    digitalWrite(S4, LOW);

    //Set light to green
    digitalWrite(L4R, LOW);
    digitalWrite(L4G, HIGH);
    digitalWrite(L4O, LOW);
  }


  // Detecting the train to stop it
  detect = digitalRead(sensor2B);

  //If the train is detected AND he need to stop THEN
  if(detect == LOW && stop2 == true) {
    //Serial.println("Stopping until restart");
    digitalWrite(F2A, HIGH);
    digitalWrite(F2B, HIGH);
    digitalWrite(S2, HIGH);
    stop2 = false;
  }
// ------------------------------------------------------- BLOCK 2 ENDS ------------------------------------------------------------------------------------

// ------------------------------------------------------- BLOCK 3 BEGIN ----------------------------------------------------------------------------------
  //Set as occupied
  detect = digitalRead(sensor3A);
  //delay(1000);
  if(detect == LOW) {
    //Set the 2 follwoings to the precedent block
    stop2 = true;
    Serial.println("stop2 = true");

    //Set light to red
    digitalWrite(L2R, HIGH);
    digitalWrite(L2G, LOW);
    digitalWrite(L2O, LOW);

    //Set the 2 followings to the before-precedent block
    stop1 = false;
    digitalWrite(F1A, LOW);
    digitalWrite(F1B, LOW);
    digitalWrite(S1, LOW);

    //Set light to green
    digitalWrite(L1R, LOW);
    digitalWrite(L1G, HIGH);
    digitalWrite(L1O, LOW);
  }


  // Detecting the train to stop it
  detect = digitalRead(sensor3B);

  //If the train is detected AND he need to stop THEN
  if(detect == LOW && stop3 == true) {
    //Serial.println("Stopping until restart");
    digitalWrite(F3A, HIGH);
    digitalWrite(F3B, HIGH);
    digitalWrite(S3, HIGH);
    stop3 = false;
  }
// ------------------------------------------------------- BLOCK 3 ENDS ------------------------------------------------------------------------------------

// ------------------------------------------------------- BLOCK 4 BEGIN ----------------------------------------------------------------------------------
  //Set as occupied
  detect = digitalRead(sensor4A);
  //delay(1000);
  if(detect == LOW) {
    //Set the 2 follwoings to the precedent block
    stop3 = true;
    Serial.println("stop3 = true");

    //Set light to red
    digitalWrite(L3R, HIGH);
    digitalWrite(L3G, LOW);
    digitalWrite(L3O, LOW);

    //Set the 2 followings to the before-precedent block
    stop2 = false;
    digitalWrite(F2A, LOW);
    digitalWrite(F2B, LOW);
    digitalWrite(S2, LOW);

    //Set light to green
    digitalWrite(L2R, LOW);
    digitalWrite(L2G, HIGH);
    digitalWrite(L2O, LOW);
  }


  // Detecting the train to stop it
  detect = digitalRead(sensor4B);

  //If the train is detected AND he need to stop THEN
  if(detect == LOW && stop4 == true) {
    //Serial.println("Stopping until restart");
    digitalWrite(F4A, HIGH);
    digitalWrite(F4B, HIGH);
    digitalWrite(S4, HIGH);
    stop4 = false;
  }
// ------------------------------------------------------- BLOCK 4 ENDS ------------------------------------------------------------------------------------
}
}

void orange() {
  digitalWrite(L1O, HIGH);
  digitalWrite(L2O, HIGH);
  digitalWrite(L3O, HIGH);
  digitalWrite(L4O, HIGH);
  delay(1000);
  digitalWrite(L1O, LOW);
  digitalWrite(L2O, LOW);
  digitalWrite(L3O, LOW);
  digitalWrite(L4O, LOW);
  delay(1000);
}

void seton() {
  digitalWrite(F1A, LOW);
  digitalWrite(F1B, LOW);
  digitalWrite(S1, LOW);

  digitalWrite(F2A, LOW);
  digitalWrite(F2B, LOW);
  digitalWrite(S2, LOW);

  digitalWrite(F3A, LOW);
  digitalWrite(F3B, LOW);
  digitalWrite(S3, LOW);

  digitalWrite(F4A, LOW);
  digitalWrite(F4B, LOW);
  digitalWrite(S4, LOW);
}
