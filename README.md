# A-TBS
![Screenshot of the result here](screenshot.jpg)

A REALLY cheap DIY Train Blocking System. Compatible with any devices MFX or DCC. Easy to setup and flexible.

## Why
The Märklin train blocking system is really expensive and very restrictive. This is completly open source and really cheaper than the original more than 300€ cheaper. And there is more, this is can be unmount, recycled, repeared, transformed, upgraded, etc.

## Build from source!
You are a maker? You want to build it by yourself? Here you are!

Go on the [BUILD.md](BUILD.md) file to build your own!

## Troubleshooting
Go on the end of the [BUILD.md](./BUILD.md) file to get the common issues, if not solved you can send an [issue](https://gitea.com/USER/PROJECT/issues).

## Contribute!
You want to contriubte? Thanks! You can get all the information at [CONTRIBUTING guide](./CONTRIBUTING.md)

## License
This project is under GNU GENERAL PUBLIC LICENSE, to get more information go to [LICENSE file](./LICENSE)
